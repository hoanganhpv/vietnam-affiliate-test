const videoContainers = document.querySelectorAll('.video');
const iframe = document.createElement('iframe');

iframe.src = 'https://www.youtube.com/embed/LM_A5Vz54jg?si=r43pw67HxQBASl18';
iframe.width = '100%'; 
iframe.height = '400'; 
iframe.frameBorder = '0'; 

videoContainers.forEach(function(videoContainer) {
    videoContainer.appendChild(iframe.cloneNode(true));
});