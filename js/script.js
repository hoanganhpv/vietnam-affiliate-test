const toggleBtn = document.querySelector(".nav-toggle");
const navContent = document.querySelector(".nav-content");

toggleBtn.addEventListener("click", () => {
    navContent.classList.toggle("active");
});